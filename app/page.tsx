"use client";

import { Canvas } from "@react-three/fiber";
import { PerspectiveCamera, Environment, OrbitControls, CameraControls } from "@react-three/drei";
import { Suspense } from "react";

import { FbxModel, Loader } from "./components";

const Home = () => {
  return (
    <Canvas flat style={{ height: '100vh', width: '100vw' }} >
      <ambientLight intensity={5} />
      <Suspense fallback={<Loader />}>
        <FbxModel name='head.fbx' scale={25} />
        <OrbitControls makeDefault autoRotate autoRotateSpeed={1} minPolarAngle={0} maxPolarAngle={Math.PI / 2} />
        <Environment preset="city" background blur={1} />
        <PerspectiveCamera makeDefault position={[10, 10, 20]} />
        <CameraControls makeDefault minPolarAngle={0} maxPolarAngle={Math.PI / 2} />
      </Suspense>
    </Canvas>
  )
}



export default Home