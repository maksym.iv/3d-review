"use client";

import { Canvas, useLoader } from "@react-three/fiber";
import { PerspectiveCamera, Environment, OrbitControls } from "@react-three/drei";
import { Suspense } from "react";
import { GlbModel, Loader } from "../components";

const Glb = () => {
  return (
    <>
      <Canvas flat style={{ height: '100vh', width: '100vw' }} >
        <Suspense fallback={<Loader />}>
          <OrbitControls makeDefault autoRotate autoRotateSpeed={1} minPolarAngle={0} maxPolarAngle={Math.PI / 2} />
          <GlbModel />
          <Environment preset="city" background blur={1} />
          <PerspectiveCamera makeDefault position={[0, 0, 20]} />
        </Suspense>
      </Canvas>
    </>
  )
}

export default Glb