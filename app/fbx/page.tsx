"use client";

import { Canvas, useLoader } from "@react-three/fiber";
import { PerspectiveCamera, Environment, OrbitControls, CameraControls, useFBX } from "@react-three/drei";
import { Suspense } from "react";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader.js";
import { FbxModel, Loader } from "../components";

const Fbx = () => {
  return (
    <Canvas flat style={{ height: '100vh', width: '100vw' }} >
      {/* <Loader /> */}
      <ambientLight intensity={5} />
      <Suspense fallback={<Loader />}>
        <FbxModel name='cat.fbx' scale={5} />
        <OrbitControls makeDefault autoRotate autoRotateSpeed={1} minPolarAngle={0} maxPolarAngle={Math.PI / 2} />
        <Environment preset="city" background blur={1} />
        <PerspectiveCamera makeDefault position={[10, 10, 20]} />
        <CameraControls makeDefault minPolarAngle={0} maxPolarAngle={Math.PI / 2} />
      </Suspense>
    </Canvas>
  )
}

export default Fbx