"use client"

import { ReactNode, useEffect } from "react";

const Layout = ({ children }: { children: ReactNode | ReactNode[] }) => {
  useEffect(() => {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker
        .register('/service-worker.js')
        .then((registration) => console.log('scope is: ', registration.scope));
    }
  }, []);

  return children;
}

export default Layout;