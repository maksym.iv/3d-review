import { Html, useProgress } from "@react-three/drei";
import Image from "next/image";

const Loader = () => {

  return   (
    <Html center>
      <Image
        style={{
          position: 'absolute',
          top: 20,
          width: 50,
          height: 50,
          zIndex: 1
        }}
        src={'loading.gif'}
        height={8}
        width={8}
        alt='loader'
        unoptimized={true}
      />
    </Html>
  )
}

export default Loader;