"use client";

import { useLoader } from "@react-three/fiber"
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader.js";

const GlbModel = () => {
    const obj = useLoader(GLTFLoader, 'napoleon.glb');
console.log('obj :>> ', obj);
    return <primitive object={obj.scene} scale={15} dispose={null} />;
}

export default GlbModel;