"use client";

import { useLoader } from "@react-three/fiber"
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader.js";

interface FbxModelProps {
    name: string;
    scale: number;
}

const FbxModel = ({ name, scale }: FbxModelProps) => {
    const fbx = useLoader(FBXLoader, name);

    return <primitive object={fbx} scale={scale} />;
}

export default FbxModel;