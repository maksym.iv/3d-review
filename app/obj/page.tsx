"use client";

import * as THREE from "three";
import { Canvas, useLoader } from "@react-three/fiber";
import { PerspectiveCamera, Environment, OrbitControls, CameraControls, } from "@react-three/drei";
import { Suspense } from "react";
import { OBJLoader } from "three/examples/jsm/loaders/OBJLoader.js";
import { DDSLoader } from "three/examples/jsm/loaders/DDSLoader.js";
import { MTLLoader } from "three/examples/jsm/loaders/MTLLoader.js";
import { useProgress } from '@react-three/drei'
import { Loader } from "../components";

THREE.DefaultLoadingManager.addHandler(/\.dds$/i, new DDSLoader());

const Scene = () => {
  const colorMap = useLoader(THREE.TextureLoader, 'apple/textured_0_SqBC5Ew3.jpg')
  const materials = useLoader(MTLLoader, 'apple/textured.mtl')

  const onProgress = (loader: OBJLoader) => {
    materials?.preload();
    loader.setMaterials(materials);
  }

  const obj = useLoader(OBJLoader, "apple/textured.obj", onProgress, (a) => console.log('a :>> ', a));

  return (
    <mesh>
      <meshStandardMaterial map={colorMap} />
      <primitive object={obj} scale={20} />
    </mesh>
  )
};

const Obj = () => {
  const prog = useProgress()

  return (
    <>
      <Canvas flat style={{ height: '100vh', width: '100vw' }} >
        <Suspense fallback={<Loader />}>
          <ambientLight intensity={4} />
          <Scene />
          <OrbitControls />

          <Environment preset="city" background blur={1} />
          <PerspectiveCamera makeDefault position={[10, 10, 30]} />
          <CameraControls makeDefault minPolarAngle={0} maxPolarAngle={Math.PI / 2} />
        </Suspense>
      </Canvas>

    </>
  )
}

export default Obj