import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import Link from 'next/link'
import Image from 'next/image'

import logo from 'public/icon512_rounded.png'
import { Layout } from './components'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: '3D Review App',
  description: 'App created to review 3d files in the web',
  manifest: '/manifest.json',
  icons: {
    apple: '/icon512_maskable.png',
  },
  themeColor: '#fff'
}

const headerItems = [
  {
    href: '/fbx',
    label: 'cat.fbx'
  },
  {
    href: '/glb',
    label: 'napoleon.glb'
  },
  {
    href: '/obj',
    label: 'apples.obj'
  },
]

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {

  return (
    <html lang="en">
      <body className={inter.className}>
        <header>
          <Link href='/'>
            <Image
              style={{
                width: 50,
                height: 50,
                zIndex: 1
              }}
              src={logo}
              height={8}
              width={8}
              alt='icon'
              unoptimized={true}
            />
          </Link>

          {headerItems.map(({ href, label }) => (
            <Link href={href} key={href}>{label}</Link>
          ))}
        </header>
        <Layout>
          {children}
        </Layout>
      </body>
    </html>
  )
}
